#include <iostream>
#include <vector>

using namespace std;

void swap(vector<int> &arr, int i, int j) {
	int temp = arr[i];
	arr[i] = arr[j];
	arr[j] = temp;
}

int partition(vector<int> &arr, int s, int e) {
	int i = s;
	int k = i;
	int j = i + 1;

	int mid = (s + e) / 2;

	swap(arr, s, mid);

	while (j < e) {
		if (arr[j] < arr[s]) {
			swap(arr, i, j);
			swap(arr, k + 1, j);
			i++;
		}
		else if (j == e) {
			k++;
			j++;
		}
		j++;
	}

	swap(arr, s, i - 1);

	return i - 1;
}

void quicksort(vector<int> &arr, int s, int e) {
	if (s >= e) { return; }

	int mid = partition(arr, s, e);

	quicksort(arr, s, mid - 1);
	quicksort(arr, mid + 1, e);
}

int main()
{
	int n;
	int val;
	vector<int> arr;
	cin >> n;

	for (int i = 0; i < n; i++) {
		cin >> val;
		arr.push_back(val);
	}
	quicksort(arr, 0, n - 1);

	return 0;
}
