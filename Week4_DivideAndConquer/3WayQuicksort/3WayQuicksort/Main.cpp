#include <iostream>
#include <vector>

using namespace std;

struct partitionResult {
	int lowerEnd;
	int upperStart;
};

void swap(vector<int> &arr, int i, int j) {
	int temp = arr[i];
	arr[i] = arr[j];
	arr[j] = temp;
}

partitionResult partition(vector<int> &arr, int s, int e) {
	int i = s;
	int k = i;
	int j = i + 1;

	int mid = (s + e) / 2;

	swap(arr, s, mid);

	while (j <= e) {
		if (arr[j] < arr[s]) {
			i++;
			k++;
			swap(arr, i, j);

			if (k > i) {
				swap(arr, k, j);
			}
		}
		else if (arr[j] == arr[s]) {
			k++; 
			swap(arr, k, j);
		}
		j++;
	}

	swap(arr, s, i);

	partitionResult result = partitionResult();
	result.lowerEnd = i -1;	// value starts from the right of i but we have swapped this with the pivot
	result.upperStart = k + 1;	// same goes for k

	return result;
}

void quicksort(vector<int> &arr, int s, int e) {

	if (s >= e) { return; }

	partitionResult result = partition(arr, s, e);

	quicksort(arr, s, result.lowerEnd);
	quicksort(arr, result.upperStart, e);
}

int main()
{
	int n;
	int val;
	vector<int> arr;
	cin >> n;

	for (int i = 0; i < n; i++) {
		cin >> val;
		arr.push_back(val);
	}
	quicksort(arr, 0, n - 1);

	return 0;
}