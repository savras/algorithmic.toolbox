// NumberOfInverses.cpp
// Pretty much counts the number of times a larger value has to be swapped with a smaller value
// during sorting.
// **Change int to long int
#include <iostream>
#include <vector>
#define ARRAY_OFFSET 1;		
using namespace std;

void merge(vector<int> &arr, int s, int e, int mid, int &result) {
	vector<int> temp(arr);
	int ptr = mid + 1;

	for (int i = s; i <= e; i++) {
		// Empty the remaining array from temp[ptr] to temp[e]
		if (s > mid) {
			arr[i] = temp[ptr];
			ptr++;
			continue;
		}

		// Empty from remaining of s to e
		if (ptr > e) {
			arr[i] = temp[s];
			s++;
			continue;
		}

		// Fill and sort original array.
		if (temp[s] < temp[ptr]) {
			arr[i] = temp[s];
			s++;
		}
		else {
			// All values in subarrays arr[s] to arr[mid], and arr[mid + 1] to arr[e] are sorted.
			result += mid - i + ARRAY_OFFSET;	// All values from i to mid will be larger than temp[ptr] so we count them all.
			arr[i] = temp[ptr];
			ptr++;
		}
	}

	temp.clear();
}

void mergesort(vector<int> &arr, int s, int e, int &result) {
	if (s >= e) {
		return;
	}
	int mid = (s + e) / 2;

	mergesort(arr, s, mid, result);
	mergesort(arr, mid + 1, e, result);

	merge(arr, s, e, mid, result);
}

int main()
{
	int n;
	int val;
	int result = 0;
	vector<int> arr;
	cin >> n;

	for (int i = 0; i < n; i++) {
		cin >> val;
		arr.push_back(val);
	}
	mergesort(arr, 0, n - 1, result);

	cout << result << endl;
	return 0;
}