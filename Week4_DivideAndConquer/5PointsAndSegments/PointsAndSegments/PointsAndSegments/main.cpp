/*
 * Divide & Conquer Programming Practice
 * Question 5
 * Key learning: Binary Search to find smallest/largest value relative to another.
 */
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

#define ARRAY_OFFSET 1;

using namespace std;

struct Segment {
	int x;
	int y;

	// http://stackoverflow.com/questions/6500313/why-should-c-programmers-minimize-use-of-new?rq=1
	Segment(int x, int y) : x(x), y(y) {}
};

// http://stackoverflow.com/questions/1380463/sorting-a-vector-of-custom-objects
struct YDescending {
	bool operator() (const Segment& segmentOne, const Segment& segmentTwo) const {
		return segmentOne.y > segmentTwo.y;
	}
};

struct XDescending {
	bool operator() (const Segment& segmentOne, const Segment& segmentTwo) const {
		return segmentOne.x > segmentTwo.x;
	}
};

long getX(Segment const& segment) { return segment.x; }
long getY(Segment const& segment) { return segment.y; }

int GetMid(int start, int end) {
	// http://stackoverflow.com/questions/6735259/calculating-mid-in-binary-search
	return start + ((end - start) / 2);
	// Alternatively: ((unsigned int)low + (unsigned int)high)) >> 1;
}

int BinarySearchY(vector<int> values, int p) {
	// Descending.. e.g. 7, 6, 5, 5, 5, 4
	// we want to find the index, i, of the first value (from the left) where values[i] < p;
	// return i + ARRAY_OFFSET;
	int start = 0;
	int end = values.size() - ARRAY_OFFSET;
	int mid;

	int result = 0;
	while (start <= end) {
		if (start == end) {
			if (p <= values[start]) {
				result++;
			}
		}

		mid = start + ((end - start) / 2);

		if (p <= values[mid]) {
			start = mid + 1; 
		}
		else {
			end = mid - 1;
		}
	}
	return result + mid;
}
int BinarySearchX(vector<int> values, int p) {

	int pos = -1;
	int start = 0;
	int end = values.size() - 1;
	int mid;

	while (start <= end) {
		mid = GetMid(start, end);
		if (p < values[mid])
		{
			start = mid + 1;
		} 
		else {	// This takes care of duplicate minimum value of x that is <= p
			end = mid - 1;
			pos = mid;
		}
	}

	return pos;
}

void Process(vector<Segment> segments, vector<int> points) {
	//sort(points.begin(), points.end());	// Sorting this allows for short cuts (i.e. skippng all values of p after the one where startIndex is >= segments.size()
	
	int startIndex;
	int result;
	for (int p = 0; p < points.size(); p++) {	// O(m)
		startIndex = 0;
		sort(segments.begin(), segments.end(), XDescending());

		vector<int> xValues;
		transform(segments.begin(), segments.end(), back_inserter(xValues), getX);
		startIndex = BinarySearchX(xValues, points[p]);	// Look for min x where x >= points[p];
		
		result = 0;
		if (startIndex >= 0)
		{
			sort(segments.begin() + startIndex, segments.end(), YDescending());	// O(n log n)  @ http://www.cplusplus.com/reference/algorithm/sort/
			vector<int> yValues;
			transform(segments.begin() + startIndex, segments.end(), back_inserter(yValues), getY);
			result = BinarySearchY(yValues, points[p]);
		}

		cout << result << " ";
	}
}

void main() {
	
	int n, m;
	cin >> n >> m;
	vector<Segment> segments;
	vector<int> points(m);

	int x, y;
	for (int i = 0; i < n; i++) {
		cin >> x >> y;
		segments.push_back(Segment(x, y));
	}

	for (int i = 0; i < m; i++) {
		cin >> points[i];
	}

	Process(segments, points);
}

// Old code
// Slower
//for (int i = startIndex; i < segments.size(); i++) {	// At most O(n)
//	if (points[p] < segments[i].x)
//	{
//		startIndex++;
//		continue;
//	}
//	break;
//}

//for (int i = startIndex; i < segments.size(); i++) {	// At most O(n)
//	if (points[p] <= segments[i].y)
//	{
//		result++;
//		continue;
//	}
//	break;
//}