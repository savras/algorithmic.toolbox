#include <iostream>
#include <vector>
#include <limits.h>
#include <algorithm>

using namespace std;

int GetMinValue(const vector<int>& arr, int i) {
	int step = arr[i - 1];

	if (i % 2 == 0) {
		step = min(arr[i / 2], step);
	}
	
	if(i % 3 == 0) {
		step - min(arr[i / 3], step);
	}

	return step + 1;
}

int GetMinStep(vector<int>& arr, int n) {
	int minValue = arr[1];
	for (int i = 2; i < arr.size(); i++) {
		minValue = GetMinValue(arr, i);
		arr[i] = minValue;
	}

	return minValue;
}

void PrintStep(const vector<int>& arr, int n) {
	int current = n;

	vector<int> result;
	vector<int>::iterator it; 
	
	int min;
	while (current > 0) {		
		it = result.begin();

		n = current;
		result.insert(it, n);
		
		min = INT_MAX;
		if (arr[n - 1] < min) {
			min = arr[n - 1];
			current = n - 1;
		}
		
		if (n % 2 == 0) {
			if (arr[n / 2] < min) {
				min = arr[n / 2];
				current = n / 2;
			}
		}
		
		if (n % 3 == 0) {
			if (arr[n / 3] < min) {
				min = arr[n / 3];
				current = n / 3;
			}
		}
	}

	for (int i = 0; i < result.size(); i++) {
		cout << result[i] << " ";
	}
}

void main() {
	int n;
	cin >> n;
	vector<int> arr(n + 1);
	arr[1] = 0;

	int minStep = GetMinStep(arr, n);
	cout << minStep << endl;

	PrintStep(arr, n);
}