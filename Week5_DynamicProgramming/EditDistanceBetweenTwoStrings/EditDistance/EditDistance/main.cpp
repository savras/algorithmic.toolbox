#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

void buildArray(vector<vector<int>>& arr, int rowLength, int colLength) {
	const int rowZero = 1;
	for (int i = 0; i <= rowLength; i++) {
		arr.push_back(vector<int>(colLength + rowZero));
	}

	for (int i = 1; i <= colLength; i++) {
		arr[0][i] = i;
	}

	for (int i = 1; i <= rowLength; i++) {
		arr[i][0] = i;
	}
}


void solveArray(vector<vector<int>>& arr, int rowLength, int colLength, string rowWord, string colWord) {
	const int offset = 1;
	for (int c = 1; c < colLength + offset; c++) {
		for (int r = 1; r < rowLength + offset; r++){
			int topLeft = arr[r - 1][c - 1];
			int left = arr[r][c - 1];
			int top= arr[r - 1][c];
			int cost = 1;

			int minVal = min(left, top);

			// Coming from top left. If rowWord[r] == colWord[c] then we don't add 1 to the value.
			if (minVal >= topLeft) {
				minVal = topLeft;
				if (rowWord[r - offset] == colWord[c - offset]) {
					cost = 0;
				}
			}
			minVal += cost;
			arr[r][c] = minVal;
		}
	}
}

int main()
{
	string rowWord;
	string colWord;

	getline(cin, rowWord);
	getline(cin, colWord);

	int colLength = colWord.length();
	int rowLength = rowWord.length();
	
	vector<vector<int>> arr; 
	buildArray(arr, rowLength, colLength);
	solveArray(arr, rowLength, colLength, rowWord, colWord);

	cout << arr[rowLength - 1][colLength - 1];

	return 0;
}