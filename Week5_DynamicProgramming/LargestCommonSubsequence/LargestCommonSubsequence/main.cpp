/*
* Refactor 1: use size_t:
*	http://stackoverflow.com/questions/1951519/when-should-i-use-stdsize-t
* Refactor 2: don't use vector<vector<vector<int>>> (Not done):
*	http://stackoverflow.com/questions/33028073/c-3d-array-declaration-using-vector
* Alternatives for 3D Array
* 1) Boost library for 3D Array:
*  http://stackoverflow.com/questions/1946830/multidimensional-variable-size-array-in-c
* 2) 3D Array with mapping:
*  http://stackoverflow.com/questions/10238699/dynamically-allocating-3d-array
*  arr[x + width * (y + depth * z)], where width is the max X count, depth is max Z index + 1 (for zero based) OR count
*/

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using std::cin;
using std::cout;
using std::string;
using std::vector;
using std::max;

void buildArray(vector<vector<vector<int>>>& arr,
				vector<int> a1, vector<int> a2, vector<int> a3,
				int l1Len, int l2Len, int l3Len) {
	const int offset = 1;
	for (size_t i = 1; i <= l1Len; i++)
	{
		for (size_t j = 1; j <= l2Len; j++)
		{
			for (size_t k = 1; k <= l3Len; k++)
			{
				if ((a1[i - offset] == a2[j - offset]) && 
					(a2[j - offset] == a3[k - offset]) && 
					(a1[i - offset] == a3[k - offset])) {
					arr[i][j][k] = arr[i - 1][j - 1][k - 1] + 1;
				}
				else {
					arr[i][j][k] = max(arr[i - 1][j][k], (max(arr[i][j - 1][k], arr[i][j][k - 1])));
				}
			}
		}
	}
}

int main() {
	int temp;
	vector<int> a1, a2, a3;
	int l1, l2, l3;

	cin >> l1;
	for (size_t i = 0; i < l1; i++) {
		cin >> temp;
		a1.push_back(temp);
	}
	cin >> l2;
	for (size_t i = 0; i < l2; i++) {
		cin >> temp;
		a2.push_back(temp);
	}
	cin >> l3;
	for (size_t i = 0; i < l3; i++) {
		cin >> temp;
		a3.push_back(temp);
	}

	const int offset = 1;

	vector<vector<vector<int>>> arr(l1 + offset, vector<vector<int>>(l2 + offset, vector<int> (l3 + offset)));
	buildArray(arr, a1, a2, a3, l1, l2, l3);

	cout << arr[l1][l2][l3];
	return 1;
}
