#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

void initializeArray(vector<vector<int>>& arr, int W, int n) {
	for (int i = 0; i < n; i++) {	
		arr[i] = vector<int>(W + 1);	// Account for column 0
	}
	
	// Fill columns for row 0
	for (int i = 0; i <= W; i++) {
		arr[0][i] = 0;
	}

	// Fill rows for column 0
	for (int i = 0; i < n; i++) {
		arr[i][0] = 0;
	}
}

int getBestValueForWeight(const vector<int>& weights, vector<vector<int>>& arr, int i, int w) {
	arr[i][w] = arr[i][w - 1];

	int currentWeight = weights[i];
	int value;

	if (currentWeight <= w) {	// There's still space to fill w with gold bar n. Remember, in this question, weight == value.
		value = (arr[i - 1][w - currentWeight] + currentWeight);	// Optimal value without weight of current item + value of current item
	}
	value = max(value, arr[i - 1][w]);

	return value;
}

void main() {
	int W;
	cin >> W;

	int n;	// No. of gold bars
	cin >> n;
	
	vector<int> weights;	// Weight = value of gold
	weights.push_back(0);	// Since we fill row and column 0 with zeroes.

	int val;
	for (int i = 0; i < n; i++) {
		cin >> val;
		weights.push_back(val);
	}

	sort(weights.begin(), weights.end());

	vector<vector<int>> arr(weights.size());
	initializeArray(arr, W, weights.size());

	for (int i = 1; i < weights.size(); i++) {
		for (int j = 1; j <= W; j++) {
			arr[i][j] = getBestValueForWeight(weights, arr, i, j);
		}
	}

	cout << arr[n][W];
}