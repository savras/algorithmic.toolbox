#include <iostream>
#include <algorithm>
#include <cassert>
#include <vector>
#include <string>

using std::cin;
using std::cout;
using std::vector;
using std::string;
using std::numeric_limits;
using std::min;
using std::max;

int evaluate(int right, int left, char op) {
	int result;
	switch (op) {
		case '*': 	{
			result = right * left;
			break;
		}
		case '+':	{
			result = right + left;
			break; 
		}
		case '-': {
			result = right - left;
			break;
		}
		default:
			assert(0);
	}

	return result;
}

void fillDiagonal(const string& expr, int length, vector<vector<int>>& minArr, vector<vector<int>>& maxArr) {
	int value;
	for (int i = 0; i < length; i++) {
		value = expr.at(i * 2) - '0';
		minArr[i][i] = value;
		maxArr[i][i] = value;
	}
}

void getMinMax(vector<vector<int>>& minArr, vector<vector<int>>& maxArr, int row, int col, const vector<char>& operators) {
	char op;
	int result;		
	int minVal = INT_MAX;
	int maxVal = INT_MIN;

	int val1, val2, val3, val4;

	// Recurrence relation:
	// array[row][i] Op array[i + 1][col] gives all possible values of left and right
	// sub expressions for all operators. Note that this is calculates all possible values for each operator.
	for (int i = row; i < col; i ++) {
		op = operators[i];
		val1 = evaluate(minArr[row][i], minArr[1 + i][col], op);
		val2 = evaluate(maxArr[row][i], minArr[1 + i][col], op);
		val3 = evaluate(minArr[row][i], maxArr[1 + i][col], op);
		val4 = evaluate(maxArr[row][i], maxArr[1 + i][col], op);

		minVal = min(minVal, min(val1, min(val2, min(val3, val4))));
		maxVal = max(maxVal, max(val1, max(val2, max(val3, val4))));
	}

	minArr[row][col] = minVal;
	maxArr[row][col] = maxVal;
}

void buildArray(string expr, int length, vector<vector<int>>& minArr, vector<vector<int>>& maxArr) {
	fillDiagonal(expr, length, minArr, maxArr);

	vector<char> operators;
	for (int i = 0; i < expr.length(); i++) {
		if (i % 2 == 1)
		{
			operators.push_back(expr[i]);
		}
	}

	for (int col = 1; col < length; col++) {
		for (int row = 0; row < length - col; row++) {
			getMinMax(minArr, maxArr, row, row + col, operators);
		}
	}
}

int main()
{
	string expr;
	cin >> expr;

	int exprLength = expr.length();
	int length = (exprLength / 2) + 1;

	vector<vector<int>> minArr(length, vector<int>(length, 0));
	vector<vector<int>> maxArr(length, vector<int>(length, 0));
	
	buildArray(expr, length, minArr, maxArr);

	cout << maxArr[0][length - 1];

	return 0;
}